import { Application } from "express";
import CritereController from "./controllers/CritereController";
import CompetenceController from "./controllers/CompetenceController";
import HomeController from "./controllers/HomeController";
import LoginController from "./controllers/LoginController";
import RegisterController from "./controllers/RegisterController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });

    app.get('/competence', (req, res) =>
    {   
        CompetenceController.competence(req, res);
    });

    app.get('/critere/:id', (req, res) =>
    {
        req.params.id
        CritereController.critere(req, res);
    });

    app.post('/critere/:id', (req, res) =>
    {
        req.params.id
        CritereController.checkbox(req, res);
    });
    app.get('/login', (req, res) =>
    {
        LoginController.showFormLog(req, res);
    });
    app.get('/register', (req, res) =>
    {
        RegisterController.showFormReg(req, res);
    });
    app.post('/register', (req, res) =>
    {
        RegisterController.enregister(req, res);
    });
    app.get('/register', (req, res)=>
    {
        RegisterController.showFormRegEchec(req, res);
    });
}
